# NextPCB Plug-in for Kicad

### Send your layout to NextPCB for quick production as fast as 24 hours with just one click.
Install this plugin for the high-quality PCB and PCB Assembly service, NextPCB is ready to plug into your awesome project.

When you click the NextPCB Plug-in button, we will export these files from your project：
1. Gerber files in the correct format for production
2. IPC-Netlist file
3. Bom-file that includes all information of components
4. Pick and Place-file used in assembly service

You can click "Save to Cart" to place an order immediately after uploading the files(which usually only takes a few seconds), our engineers will double-check the files before the production.

### Installation from the official KiCad repositories
Just open the "Plugin and Content Manager" from the KiCad main menu and install the "NextPCB Plug-in for KiCad" plugin from the selection list.

### Manual Installation
You can also download the latest ZIP file from
https://gitlab.com/nextpcbofficial/NextPCB-Plug-in-for-Kicad, then open the "Plugin and Content Manager" from the main window of KiCad and install the ZIP file via "Install from File".

### About BOM
We can get all information on the components used in your design. To speed up the quotation of components, we need this information:
1. Designator (necessary)
2. Quantity (necessary)
3. MPN/Part Number (necessary)
4. Package/Footprint (necessary)
5. Manufacturer (optional)
6. Description/value (optional)


### About NextPCB
NextPCB is a Chinese company specializing in PCB prototyping and assembly, and offers a one-stop solution service that involves PCB Prototype & Batch Production, PCB Assembly (SMT), components sourcing, quality testing, global delivery, and Quick turnaround(as fast as 24 hours lead time).

As one of the sponsors of Kicard, we would like to invite you to join us NextPCB, enjoy this chance of $0 PCB and create an amazing electronic world together.

### License and attribution

This plugin is published under the [MIT License](./LICENSE) and is based on [AislerHQ/PushForKiCad](https://github.com/AislerHQ/PushForKiCad).
